export class User {
   id : string;
   type_user: string;
   name: string;
   email: string;
   password: string;
   telephone: string;
}
