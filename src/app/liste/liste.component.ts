import { Component, OnInit } from '@angular/core';
import {ApiService} from "../api.service";
import {Panne} from "../shared/panne";
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-liste',
  templateUrl: './liste.component.html',
  styleUrls: ['./liste.component.css']
})
export class ListeComponent implements OnInit {

  //list = NameTable;
  //selectedReclamation: Reclamation;
  //onSelect(reclamation: Reclamation): void {
  //this.selectedReclamation = reclamation;
//}
  constructor(private apiService:ApiService,private router:Router,
              private route : ActivatedRoute) { }

    mypanne: any;

  ngOnInit() {
     this.apiService.getRecalamtions()
      .subscribe((data) => this.mypanne = data );

     //console.log(this.mypanne);

  }



  deleteReclamation(id){
    console.log(id);
    this.apiService.deleteReclamation(id)
      .subscribe((data) => {console.log(data);
      this.apiService.getRecalamtions();
      // Refresh Data
          this.apiService.redirectTo(this.router.url);
      },
          error1 => console.log(error1));

  }

  editReclamation(id){
    this.router.navigate(['/modifier/'+'/'+id]);
  }

}
