import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  public imagesUrl2;

  constructor() { }

  ngOnInit() {
    this.imagesUrl2 = [
       './assets/images/face.png',
       './assets/images/gmail.png',
         './assets/images/insta.png',
      './assets/images/linkedin.png'
    ];
  }

}
