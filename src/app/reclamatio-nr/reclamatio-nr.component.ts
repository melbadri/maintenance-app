import { Component, OnInit } from '@angular/core';
import {ApiService} from "../api.service";
import {AlertService} from "ngx-alerts";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-reclamatio-nr',
  templateUrl: './reclamatio-nr.component.html',
  styleUrls: ['./reclamatio-nr.component.css']
})
export class ReclamatioNRComponent implements OnInit {

  constructor(private apiService: ApiService,
              private  alertService: AlertService,
              private router:Router,
              private route : ActivatedRoute) { }

    mypanne : any ;
  ngOnInit() {
    this.apiService.getSpecificRec("pending")
      .subscribe( (data) => {
        this.mypanne = data;

      });
  }

  validerReclamation(id){
     var data = { "etat" : "valide"};

    this.apiService.editReclamation(data, id)
      .subscribe( (data) => {
        this.alertService.success("Reclamation Validé");
        this.apiService.redirectTo(this.router.url);
      });

}





}
