import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReclamatioNRComponent } from './reclamatio-nr.component';

describe('ReclamatioNRComponent', () => {
  let component: ReclamatioNRComponent;
  let fixture: ComponentFixture<ReclamatioNRComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReclamatioNRComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReclamatioNRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
