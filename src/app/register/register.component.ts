import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import {AlertService} from "ngx-alerts";


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})



export class RegisterComponent implements OnInit {

  created : boolean;
  constructor(private apiService:ApiService,
              private alertService: AlertService) { }

  ngOnInit() {
   this.apiService.getUsers();
  }
  
  registerUser(user){

          if(user.password == user.confirmPassword){


          this.apiService.registerUsser(user)
            .subscribe(
              user => {
                   this.alertService.success("User created successfully");

              }, error1 => this.alertService.danger("mail already exists"));

          }else{

              this.alertService.danger("Passwords do not match");
          }
  } 


}
