import { Component, OnInit } from '@angular/core';
import {ApiService} from "../api.service";

@Component({
  selector: 'app-reclamatio-r',
  templateUrl: './reclamatio-r.component.html',
  styleUrls: ['./reclamatio-r.component.css']
})
export class ReclamatioRComponent implements OnInit {

  constructor(private apiService: ApiService) { }


    mypanne: any ;
  ngOnInit() {

    this.apiService.getSpecificRec("valide")
      .subscribe( (data) => {
        this.mypanne = data;

      });
  }


}
