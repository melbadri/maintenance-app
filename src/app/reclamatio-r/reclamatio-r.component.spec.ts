import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReclamatioRComponent } from './reclamatio-r.component';

describe('ReclamatioRComponent', () => {
  let component: ReclamatioRComponent;
  let fixture: ComponentFixture<ReclamatioRComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReclamatioRComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReclamatioRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
