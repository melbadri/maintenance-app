import { Component, OnInit } from '@angular/core';
import {ApiService} from "../api.service";
import {Observable} from "rxjs";
import {Router} from "@angular/router";


@Component({
  selector: 'app-navbar0',
  templateUrl: './navbar0.component.html',
  styleUrls: ['./navbar0.component.css']
})
export class Navbar0Component implements OnInit {

  constructor(public apiService:ApiService,
              private router:Router) { }


  ngOnInit() {
  }

  isLogged = this.apiService.getLoggedIn();

  logout(){

      this.apiService.setLoggedOut();
    this.router.navigate(['/']);

    console.log("logged out");
  }



}
