import { Component, OnInit } from '@angular/core';
import {ApiService} from "../api.service";
import {Router} from '@angular/router';
import {AlertService} from "ngx-alerts";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private apiService:ApiService,
              private router:Router,
              private alertService:AlertService) { }



  ngOnInit() {
     this.apiService.getUsers();
  }


  loginUser(user){
    this.apiService.loginUser(user).subscribe(
      (res) => {
        this.sucessAlert();

        if(user.type_user =="user"){
          this.router.navigate(['/connection']);
        }
        if(user.type_user =="technicien"){
          this.router.navigate(['/connectiontech']);
        }

        this.apiService.setLoggedIn();

      }, error => {

        this.alertService.danger("Authententification Failed !");

      });
  }


  sucessAlert(): void{
    this.alertService.success('Authentification success !');
  }

  failedAlert(): void {

    this.alertService.warning('Authentification Failed !');

  }

}
