import { Component, OnInit } from '@angular/core';
import {ApiService} from "../api.service";
import {AlertService} from "ngx-alerts";
import {Router} from '@angular/router';


@Component({
  selector: 'app-ajouter',
  templateUrl: './ajouter.component.html',
  styleUrls: ['./ajouter.component.css']
})
export class AjouterComponent implements OnInit {

  constructor(private apiService:ApiService, private alertService:AlertService, private router:Router) { }

  ngOnInit() {
  }

  addReclamation(reclamation){
    this.apiService.addReclamation(reclamation)
      .subscribe((resp) => {
        console.log(resp);
        this.sucessAlert();

        this.router.navigate(['/liste']);

      });
  }

  sucessAlert(): void{
    this.alertService.success('Reclamation ajoutée avec succès !');
  }
}
