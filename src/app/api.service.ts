import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {User} from "./shared/user";
import {Observable} from "rxjs";
import {Panne} from "./shared/panne";
import 'rxjs/add/operator/map';
import {ActivatedRoute, Router} from '@angular/router';




@Injectable({
  providedIn: 'root'
})

export class ApiService {



  constructor(private http: HttpClient,private router:Router,
              private route : ActivatedRoute) { }

              private loggedStatus: boolean =false ;


      setLoggedIn(){

        this.loggedStatus = true;
      }

  setLoggedOut(){

    this.loggedStatus = false;
  }

        getLoggedIn(){

              return this.loggedStatus;
        }


  registerUsser(user: User): Observable<User>{
   //console.log(user);
   return this.http.post<User>("http://localhost:3000/api/signup", user);

  }

  getUsers(){
  let obs =  this.http.get("http://localhost:3000/api/user");
  obs.subscribe((response) => console.log(response));
  }

  loginUser(user: User): Observable<User>{
    console.log(user);
    return this.http.post<User>("http://localhost:3000/api/login", user);

  }

//retourner les reclamations
  getRecalamtions(){
       return this.http.get("http://localhost:3000/api/reclamation");

  }
  // Get by ID
  getRecalamtion(id){
    return this.http.get("http://localhost:3000/api/reclamation"+"/"+id);

  }

  //ajouter reclamation
  addReclamation(reclamation: Panne):Observable<Panne>{

    console.log(reclamation);
    return this.http.post<Panne>("http://localhost:3000/api/reclamation", reclamation);

  }

  //supprimer reclamation
  deleteReclamation(id){


    return this.http.delete("http://localhost:3000/api/reclamation"+"/"+id);
  }

  //editReclamation By Id

  editReclamation(data, id){

    return this.http.put("http://localhost:3000/api/reclamation"+"/"+id, data);

  }

  getSpecificRec(etat){
       return this.http.get("http://localhost:3000/api/reclamations"+"/"+etat);

  }



  // Refresh
  redirectTo(uri) {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
      this.router.navigate([uri]));
  }

}

