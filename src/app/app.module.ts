import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { RouterModule} from '@angular/router';
import { FormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCardModule, MatToolbarModule, MatListItem } from '@angular/material';
import { MatInputModule } from '@angular/material';
import {MatRadioModule} from '@angular/material/radio';
import { AngularFontAwesomeModule } from 'angular-font-awesome';





import { AppComponent } from './app.component';
import { Navbar0Component } from './navbar0/navbar0.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';

import {AuthGuard} from "./auth.guard";
import { ApiService } from './api.service';

import { SliderModule } from './slider/slider.module';
import { ConnectionComponent } from './connection/connection.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { AjouterComponent } from './ajouter/ajouter.component';
import { ListeComponent } from './liste/liste.component';
import { ChoixComponent } from './choix/choix.component';
import { ConnectionAdminComponent } from './connection-admin/connection-admin.component';
import { ConnectionTechComponent } from './connection-tech/connection-tech.component';
import { FooterComponent } from './footer/footer.component';
//import { SliderComponent } from './slider/slider.component';
//import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// Import your library
import { AlertModule } from 'ngx-alerts';
import { ModifierRecComponent } from './modifier-rec/modifier-rec.component';
import { ReclamatioRComponent } from './reclamatio-r/reclamatio-r.component';
import { ReclamatioNRComponent } from './reclamatio-nr/reclamatio-nr.component';

@NgModule({
  declarations: [
    AppComponent,
    Navbar0Component,
    RegisterComponent,
    LoginComponent,
    ConnectionComponent,
    AboutComponent,
    ContactComponent,
    AjouterComponent,
    ListeComponent,
    ChoixComponent,
    ConnectionAdminComponent,
    ConnectionTechComponent,
    FooterComponent,
    ModifierRecComponent,
    ReclamatioRComponent,
    ReclamatioNRComponent,
    //SliderComponent
    //SliderComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatCardModule,
    SliderModule,
    MatButtonModule,
    MatToolbarModule,
    MatInputModule,
    MatRadioModule,
    AngularFontAwesomeModule,
    AlertModule.forRoot({maxMessages: 5, timeout: 6000, position: 'right'}),
    RouterModule.forRoot([
      {path:"register", component: RegisterComponent, },
      {path:"login", component: LoginComponent},
      {path:"connection", component: ConnectionComponent},
      {path:"about", component: AboutComponent},
      {path:"contact", component: ContactComponent},
      {path:"creat", component: AjouterComponent},
      {path:"liste", component: ListeComponent},
      {path:"choix", component: ChoixComponent},
      {path:"resolu", component: ReclamatioRComponent},
      {path:"nresolu", component: ReclamatioNRComponent},
      {path:"modifier/:id", component: ModifierRecComponent},
      {path:"connectionadmin", component: ConnectionAdminComponent},
      {path:"connectiontech", component: ConnectionTechComponent}
    ])
  ],
  providers: [ApiService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
