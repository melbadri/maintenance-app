import { Component, OnInit } from '@angular/core';
import {ApiService} from "../api.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AlertService} from "ngx-alerts";

@Component({
  selector: 'app-connection-tech',
  templateUrl: './connection-tech.component.html',
  styleUrls: ['./connection-tech.component.css']
})
export class ConnectionTechComponent implements OnInit {

  constructor(private apiService:ApiService,
              private router:Router,
              private route : ActivatedRoute,
              private alertService:AlertService) { }

             mypanne: any ;

  ngOnInit() {

    this.apiService.getRecalamtions()
      .subscribe((data) => this.mypanne = data );

  }

  validerReclamation(id){

    var data  = { "etat" : "valide"};
    this.apiService.editReclamation(data, id)
      .subscribe( (data) => {
        this.alertService.success("Reclamation Validé");
        this.apiService.redirectTo(this.router.url);
      });

 }

}
