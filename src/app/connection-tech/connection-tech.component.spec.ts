import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectionTechComponent } from './connection-tech.component';

describe('ConnectionTechComponent', () => {
  let component: ConnectionTechComponent;
  let fixture: ComponentFixture<ConnectionTechComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectionTechComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectionTechComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
