import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierRecComponent } from './modifier-rec.component';

describe('ModifierRecComponent', () => {
  let component: ModifierRecComponent;
  let fixture: ComponentFixture<ModifierRecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierRecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierRecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
