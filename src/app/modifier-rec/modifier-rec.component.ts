import { Component, OnInit } from '@angular/core';
import {ApiService} from "../api.service";
import {ListeComponent} from "../liste/liste.component";
import {ActivatedRoute, Router} from "@angular/router";
import {AlertService} from "ngx-alerts";


@Component({
  selector: 'app-modifier-rec',
  templateUrl: './modifier-rec.component.html',
  styleUrls: ['./modifier-rec.component.css']
})
export class ModifierRecComponent implements OnInit {

  constructor(private apiService: ApiService,
              private activatedRoute: ActivatedRoute,
              private alertService:AlertService) { }

  public id: string;
  reclamation : any ="" ;


  ngOnInit() {
    // get the id of the Rec From URL
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    console.log(this.id);
    this.apiService.getRecalamtion(this.id)
      .subscribe( (data) => {
            this.reclamation = data;
      });

  }


  editReclamation(data, id){

  this.apiService.editReclamation(data, id)
    .subscribe((data) =>

    {this.alertService.success('Modification effectué !');

    });

}




}
