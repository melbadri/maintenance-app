import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'front0';
  public imagesUrl;
  public imagesUrl2;

  ngOnInit() {
    this.imagesUrl = [
      './assets/images/ecout.png',
       './assets/images/qualite.png',
       './assets/images/securite.png',
         './assets/images/env.png',
      './assets/images/comp.png'
    ];
    this.imagesUrl2 = [
       './assets/images/qualite.png',
       './assets/images/securite.png',
         './assets/images/env.png',
      './assets/images/comp.png'
    ];
  }
}
